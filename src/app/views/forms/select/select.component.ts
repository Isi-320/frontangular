import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  
  @Output() fullname = localStorage.getItem('fullname');
  @Output() gender = localStorage.getItem('gender');
  @Output() birthdate = localStorage.getItem('birthdate');
  @Output() address = localStorage.getItem('address');
  @Output() city = localStorage.getItem('city');
  @Output() province = localStorage.getItem('province');
  @Output() countryresidence = localStorage.getItem('countryresidence');
  @Output() passportNumber = localStorage.getItem('passportNumber');
  @Output() placeIssue = localStorage.getItem('placeIssue');
  @Output() trip = localStorage.getItem('trip');
  @Output() valueTravellingChild = localStorage.getItem('valueTravellingChild');
  @Output() ifyes = localStorage.getItem('ifyes');
  @Output() kinship = localStorage.getItem('kinship');
  @Output() mainPurpose = localStorage.getItem('mainPurpose');
  @Output() nameMeansTransport = localStorage.getItem('nameMeansTransport');
  @Output() detailsMeansTransport = localStorage.getItem('detailsMeansTransport');

  constructor() { }

  ngOnInit(): void {
  }
}
